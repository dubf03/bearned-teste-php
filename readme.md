

##1-Clonando o projeto

    git clone git@bitbucket.org:dubf03/bearned-teste-php.git bearned

##2-Permissões de pasta

    chmod -R 775 bearned/
    chmod -R 775 bearned/storage
    chmod -R 775 bearned/bootstrap

##3-Instlando dependências

    cd bearned
    composer install

##4-Criar banco de dados   
    - Criar um banco de dados 'bearned'
    - Configurar o arquivo .env para o banco local
    Executar:
    php artisan migrate

##5-Iniciando a aplicação
    php artisan serve


## Dependências
    - Laravel 5.6
    - JQuery
    