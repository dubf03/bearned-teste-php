<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Number;

class DashBoardController extends Controller
{

    private $number;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Number $number)
    {   
        $this->middleware('auth');
        $this->number = $number;
        
    }

    public function home()
    {
        $list = $this->number->all();
        $data = ['list' => $list];
        return view('home', $data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $file = file_get_contents("http://pox.globo.com/rss/g1/economia/");
        $news = simplexml_load_string($file);

        $list = $this->number->orderBy('created_at', 'desc')->limit(3)->get();
        $data = [
            'list' => $list,
            'news' => $news->channel
        ];
        
        return view('dashboard.home', $data);
    }

    public function store(Request $request){
        $data = $request->all();
        $number =  $this->number->create($data);
        return redirect('home')->with('success', 'Novo número adcionado!');
    }

    public function update(Request $request){
        $data = $request->all();
        $number = $this->number->find($data['id']);
        $number->title = $data['title'];
        $number->value = $data['value'];
        $number->save();

        return redirect('home')->with('success', 'Item atualizado');
    }

    public function delete($id){
        $number = $this->number->find($id);
        $number->delete();
        return redirect('home')->with('success', 'Item excluído.');
    }
}
