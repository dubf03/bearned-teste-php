<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'DashBoardController@home')->name('home');
Route::post('/new', 'DashBoardController@store')->name('new');
Route::post('/update', 'DashBoardController@update')->name('update');
Route::get('/delete/{id}', 'DashBoardController@delete')->name('delete');
Route::get('/dashboard', 'DashBoardController@dashboard')->name('dashboard');
