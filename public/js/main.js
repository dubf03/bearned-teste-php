let showFormItem = function(id, display){
    event.preventDefault();
    item = "#form-item-"+id; 
    let form = document.querySelector(item);
    form.style.display = display;
}

let showFormNew = function(display){
    event.preventDefault();
    let form = document.querySelector('.form-add-item');
    form.style.display = display;
}

let callbackFunction = function(data) {
    weather = data.query.results.channel;
    temp = Math.floor((weather.item.condition.temp - 32) / 1.8);
    city = weather.location.city;
    setHour();
    document.querySelector('footer .weather span').innerHTML = city + " - " + temp.toString() + '&deg;';
};

let setHour = function(){
    timeFull = new Date();
    let min;
    if(timeFull.getMinutes() >= 10) {
        min = timeFull.getMinutes();
    }else {
        min =  "0"+timeFull.getMinutes();
    }
    document.querySelector('footer .time span').innerHTML = timeFull.getHours()+":"+min;

    setInterval(function () {
        setHour();
    }, 5000);
}

jQuery(document).ready(function ($) {

    setInterval(function () {
        moveRight();
    }, 10000);
    
      var slideCount = $('#slider ul li').length;
      var slideWidth = $('#slider ul li').width();
      var slideHeight = $('#slider ul li').height();
      var sliderUlWidth = slideCount * slideWidth;
      
      $('#slider').css({ width: slideWidth, height: slideHeight });
      
      $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
      
      $('#slider ul li:last-child').prependTo('#slider ul');
  
      function moveLeft() {
          $('#slider ul').animate({
              left: + slideWidth
          }, 200, function () {
              $('#slider ul li:last-child').prependTo('#slider ul');
              $('#slider ul').css('left', '');
          });
      };
  
      function moveRight() {
          $('#slider ul').animate({
              left: - slideWidth
          }, 200, function () {
              $('#slider ul li:first-child').appendTo('#slider ul');
              $('#slider ul').css('left', '');
          });
      };
  
  });    
  