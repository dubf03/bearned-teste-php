<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    <style type="text/css" media="screen">
      body { font-family: Arial; }
      .hg-weather { border-radius: 3px; padding: 15px; display: inline-block; }

      .hg-weather.clear_day { background-color: #217CAF; color: white; }
      .hg-weather.clear_night { background-color: #000; color: white; }
      .hg-weather.cloudly_day, .hg-weather.fog, .hg-weather.rain, .hg-weather.storm { background-color: #ddd; }
      .hg-weather.cloudly_night { background-color: #444; color: white; }

      /*
      Classes que são adicionadas de acordo com a condição climática:

      storm (tempestade)
      snow (neve)
      hail (granizo)
      rain (chuva)
      fog (neblina)
      clear_day (ensolarado)
      clear_night (estrelado)
      cloud (nublado)
      cloudly_day (nublado com sol)
      cloudly_night (nublado com estrelas)
      none_day (erro, mas esta de dia)
      none_night (erro, mas esta de noite)
      */
    </style>
</head>
<body>
    <div id="app" class="container full-screen">
        @yield('content')
    </div>
    <footer>
        <div class="time">
            <span>11:11</span>
        </div>

        <div class="weather">
            <span>São Paulo - </span>
        </div>
    </footer>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/simpleslider.min.js') }}"></script>


    <script src="https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='são paulo, sp')&format=json&callback=callbackFunction"></script>

</body>
</html>
