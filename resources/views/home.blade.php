@extends('layouts.app')

@section('content')
<div class="container">

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@if (session('danger'))
    <div class="alert alert-danger">
        {{ session('danger') }}
    </div>
@endif

    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="title" >Gestão de Números</span>
                    <span class="btn-title">
                        <a href="/dashboard" target="_blank" class="btn btn-light">Ver Dashboard</a>
                        <button onclick="showFormNew('block')"  class="btn btn-light">Novo número +</button>
                    </span>
                </div>
                <div class="panel-body">

                    <div class="form-add-item">
                        <form method="post" name="new-item" action="/new">
                        {{ csrf_field() }}
                        <fieldset>
                            <legend>Novo número</legend>

                            <table class="table table-bordered">
                                <tr>
                                    <td>Título</td>
                                    <td>Valor</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="title" required/></td>
                                    <td><input type="number" name="value" required/></td>
                                    <td>
                                        <input class="btn btn-primary" type="submit" value="Cadastrar" />
                                        <button onclick="showFormNew('none')" class="btn btn-outline-secondary">Cancelar</button>
                                    </td>
                                </tr>
                            </table>

                        </fieldset>
                        </form>
                    </div>

                    <table class="table" id="table-items">
                        <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Título</th>
                            <th>Valor</th>
                            <th>Data/Hora</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        @foreach ($list as $item)
                            <tr>
                                <td>#{{$item->id}}</td>
                                <td>{{$item->title}}</td>
                                <td>{{$item->value}}</td>
                                <td>{{$item->created_at}}</td>
                                <td class="actions">
                                    <a href="#" onclick="showFormItem({{$item->id}}, 'block')" class="btn btn-light"> Editar </a>
                                    <a href="/delete/{{$item->id}}"  class="btn btn-light"> Excluir </a>
                                    <form action="/update" method="post" name="form-item" class="form-item" id="form-item-{{$item->id}}">
                                    {{ csrf_field() }}
                                        <fieldset>
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td>Título</td>
                                                    <td>Valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="id" value="{{$item->id}}"/>
                                                        <input type="text" name="title" value="{{$item->title}}" required/>
                                                    </td>
                                                    <td><input type="number" name="value" value="{{$item->value}}" required/></td>
                                                    <td class="form-item-actions">
                                                        <input class="btn btn-primary" type="submit" value="Atualizar" />
                                                        <button class="btn btn-secondary" onclick="showFormItem({{$item->id}}, 'none')">Cancelar</button>
                                                    </td>
                                                </tr>
                                            </table>

                                        </fieldset>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
