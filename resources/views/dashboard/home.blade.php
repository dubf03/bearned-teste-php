@extends('layouts.dashboard')

@section('content')
<div class="db-container">
    <section class="numbers col-lg-3 col-md-4">
        <ul class="list-group">
            @foreach ($list as $item)
                <li class="list-group-item">
                    <h2>{{$item->title}}</h2>
                    <span>{{$item->value}}</span>
                </li>
            @endforeach
        </ul>
    </section>
    
    
    <!-- <div id="slider" class="news"  style="width:612px; height:612px">

    </div> -->

<div id="slider" class="col-lg-9 col-md-8">
  <ul>
    @foreach ($news->item as $item)
        <li>
            <div class="content">
                <h2>{{$item->title}}</h2>
                <p>
                    {!! substr($item->description, 0, 1800) !!}...
                </p>
            </div>
        </li>
    @endforeach
  </ul>  
</div>

</div>
@endsection
